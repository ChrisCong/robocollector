//
//  BackgroundPhotoCache.swift
//  Water
//
//  Created by Christopher Cong on 10/28/15.
//  Copyright © 2015 ChrisCong. All rights reserved.
//

import Foundation
import UIKit

protocol Cache {
    // Returns the number of items in the cache
    func count() -> Int
    
    // Clears all items in the cache
    func clearCache()
    
    // Deletes all entries that have expired
    func purgeStaleData() -> Int
    
    // Saves an image with a key
    func store(image: UIImage, forKey key: String)
    
    // Retrieves and touches (extends expiration) the image for the given key
    func image(forKey key: String) -> UIImage?
    
    // Removes image from disk and/or cache for the given key
    func removeImage(forKey key: String)
}

//TODO: get all the keys to the images
public class ImageCache {
    
    static let sharedInstance = ImageCache()
    
    //MARK: Properties
    private let memCache: NSCache = NSCache()
    private let diskCachePath: String = (NSSearchPathForDirectoriesInDomains(.CachesDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as NSString).stringByAppendingPathComponent("com.chriscong.CCImageCache")
    private let ioQueue: dispatch_queue_t = dispatch_queue_create("com.chriscong.CCImageCache", DISPATCH_QUEUE_SERIAL)
    private let fileManager: NSFileManager = NSFileManager()
    
    init() {
        self.setupAppNotifications()
    }
    
    func setupAppNotifications() {
        NSNotificationCenter.defaultCenter().addObserverForName("UIApplicationDidReceiveMemoryWarningNotification",
            object: nil,
            queue: NSOperationQueue.mainQueue()) { _ in
                self.memCache.removeAllObjects()
        }
        NSNotificationCenter.defaultCenter().addObserverForName("UIApplicationWillTerminateNotification",
            object: nil,
            queue: NSOperationQueue.mainQueue()) { _ in
                self.memCache.removeAllObjects()
        }
        NSNotificationCenter.defaultCenter().addObserverForName("UIApplicationDidEnterBackgroundNotification",
            object: nil,
            queue: NSOperationQueue.mainQueue()) { _ in
                self.memCache.removeAllObjects()
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}

extension ImageCache : Cache {
    func count() -> Int {
        var newCount: Int = 0
        dispatch_sync(self.ioQueue) {
            let fileEnumerator = self.fileManager.enumeratorAtPath(self.diskCachePath)
            newCount = fileEnumerator!.allObjects.count
        }
        return newCount
    }
    
    func imageCachePath(forKey key: String) -> String {
        return self.diskCachePath.stringByAppendingFormat(key.MD5 as String)
    }
    
    //MARK: Image Exists?
    func imageExists(forKey key: String) -> Bool {
        // this is an exception to access the filemanager on another queue than ioQueue, but we are using the shared instance
        // from apple docs on NSFileManager: The methods of the shared NSFileManager object can be called from multiple threads safely.
        return NSFileManager.defaultManager().fileExistsAtPath(self.imageCachePath(forKey: key))
    }
    
    func imageExists(forKey key: String, completionBlock: ((Bool) -> Void)?) {
        dispatch_async(self.ioQueue) {
            let isCached = self.fileManager.fileExistsAtPath(self.imageCachePath(forKey: key))
            dispatch_async(dispatch_get_main_queue(), {
                completionBlock?(isCached)
            })
        }
    }
    
    //MARK: Image Retrieval
    func imageFromMemoryCache(forKey key: String) -> UIImage? {
        return self.memCache.objectForKey(key) as? UIImage
    }
    
    func imageFromDiskCache(forKey key: String) -> UIImage? {
        if let data = NSData(contentsOfFile: self.imageCachePath(forKey: key)) {
            return UIImage(data: data)
        } else {
            return nil
        }
    }
    
    func image(forKey key: String) -> UIImage? {
        if let memImage = self.imageFromMemoryCache(forKey: key) {
            return memImage
        } else if let diskImage = self.imageFromDiskCache(forKey: key) {
            self.memCache.setObject(diskImage, forKey: key, cost: Int(diskImage.cost))
            return diskImage
        } else {
            return nil
        }
    }
    
    func image(forKey key: String, completion: ((UIImage?) -> Void)) -> NSOperation? {
        if let memImage = self.imageFromMemoryCache(forKey: key) {
            completion(memImage)
            return nil
        } else{
            let operation = NSOperation()
            dispatch_async(self.ioQueue, {
                if operation.cancelled {
                    return
                }
                autoreleasepool({
                    if let diskImage = self.imageFromMemoryCache(forKey: key) {
                        self.memCache.setObject(diskImage, forKey: key, cost: Int(diskImage.cost))
                        completion(diskImage)
                    } else {
                        completion(nil)
                    }
                })
            })
            return operation
        }
    }
    
    //MARK: Store Image
    func store(image: UIImage, forKey key: String) {
        return self.store(image, imageData: nil, forKey: key)
    }
    
    func store(image: UIImage, imageData: NSData?, forKey key: String) {
        // cache in memory
        self.memCache.setObject(image, forKey: key, cost: Int(image.cost))
        
        // cache on disk
        dispatch_async(self.ioQueue) {
            
            // store image as data to save CPU cycles and image quality
            let data: NSData
            if (imageData != nil) {
                data = imageData!
            } else {
                guard let _data = image.data else {
                    return;
                }
                data = _data
            }
            
            // init directory
            if !self.fileManager.fileExistsAtPath(self.diskCachePath) {
                do {
                    try self.fileManager.createDirectoryAtPath(self.diskCachePath, withIntermediateDirectories: true, attributes: nil)
                } catch let error as NSError {
                    NSLog("\(error.localizedDescription)")
                }
            }
            
            let cachePathForKey = self.imageCachePath(forKey: key)
            self.fileManager.createFileAtPath(cachePathForKey, contents: data, attributes: nil)
        }
    }
    
    //MARK: Remove Image
    func removeImage(forKey key: String) {
        self.removeImage(forKey: key, completion: nil)
    }
    
    func removeImage(forKey key: String, completion: (() -> Void)?) {
        self.memCache.removeObjectForKey(key)
        
        dispatch_async(self.ioQueue) { () in
            do {
                try self.fileManager.removeItemAtPath(self.diskCachePath)
            } catch let error as NSError {
                NSLog("\(error.localizedDescription)")
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if completion != nil {
                   completion!()
                }
            })
        }
    }
    
    //MARK: Clean Cache
    func clearCache() {
        self.clearCache {}
    }
    
    func clearCache(completion: (() -> Void)?) {
        self.memCache.removeAllObjects()
        
        dispatch_async(self.ioQueue) { () in
            do {
                try self.fileManager.removeItemAtPath(self.diskCachePath)
                try self.fileManager.createDirectoryAtPath(self.diskCachePath, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("\(error.localizedDescription)")
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if completion != nil {
                    completion!()
                }
            })
        }
    }
    
    //TODO:
    func purgeStaleData() -> Int {
        return 0
    }
}

extension String {
    var MD5: NSString {
        //benchmarks: https://gist.github.com/mishagray/4c25056273fd9c82a813
        guard
            let data = (self as NSString).dataUsingEncoding(NSUTF8StringEncoding),
            let result = NSMutableData(length: Int(CC_MD5_DIGEST_LENGTH))
            else {
                //return original string if cannot produce MD5 hash
                return self
        }
        
        let resultBytes = UnsafeMutablePointer<CUnsignedChar>(result.mutableBytes)
        CC_MD5(data.bytes, CC_LONG(data.length), resultBytes)
        
        let iterator = UnsafeBufferPointer<CUnsignedChar>(start: resultBytes, count: result.length)
        let hash = NSMutableString()
        
        for i in iterator {
            hash.appendFormat("%02x", i)
        }
        
        return hash
    }
}

public extension UIImage {
    var cost: CGFloat {
        return self.size.height * self.size.width * self.scale * self.scale
    }
    var data: NSData? {
        let alphaInfo = CGImageGetAlphaInfo(self.CGImage)
        let hasAlpha = !(alphaInfo == CGImageAlphaInfo.None ||
            alphaInfo == CGImageAlphaInfo.NoneSkipFirst ||
            alphaInfo == CGImageAlphaInfo.NoneSkipLast)
        
        if hasAlpha {
            return UIImagePNGRepresentation(self)
        } else {
            return UIImageJPEGRepresentation(self, 1.0)
        }
    }
}