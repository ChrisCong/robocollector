//
//  RoboCollection.swift
//  RoboCollector
//
//  Created by Christopher Cong on 4/21/16.
//  Copyright © 2016 ChrisCong. All rights reserved.
//

import Alamofire

typealias Robo = String

protocol RoboCollectionDelegate: AnyObject {
    func collectionDidUpdate()
}

class RoboCollection: AnyObject {
    //MARK: Properties
    var delegate: RoboCollectionDelegate?
    var count: Int {
        get {
            return roboList.count
        }
    }
    
    private var roboList: Array<Robo> = [] //Could use a Set if we do not allow duplicate copies of robots
    private var kUserDefaultsRoboListKey = "user_defaults_robo_list"
    
    //MARK: Subscript
    subscript(index: Int) -> Robo {
        get {
            //TODO: should have some check to see if the robo exists in the ImageCache, and also set up channels to observe images are deleted
            return roboList[index]
        }
    }
    
    //MARK: Lifecycle
    init() {
        self.setupAppNotifications()
        
        if let _roboList = NSUserDefaults.standardUserDefaults().objectForKey(kUserDefaultsRoboListKey) as? Array<Robo> {
            roboList = _roboList
        }
    }
    
    convenience init(delegate: RoboCollectionDelegate) {
        self.init()
        self.delegate = delegate
    }
    
    func setupAppNotifications() {
        NSNotificationCenter.defaultCenter().addObserverForName("UIApplicationWillTerminateNotification",
                                                                object: nil,
                                                                queue: NSOperationQueue.mainQueue()) { _ in
                                                                    self.persist()
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    //MARK: Functions
    func addRobo(robo: Robo, set: RoboSet) {
        Alamofire.request(.GET, "https://robohash.org/\(robo)", parameters: ["set": set.rawValue]).responseData { [unowned self] response in
            if let data = response.data {
                if let image = UIImage(data: data) {
                    ImageCache.sharedInstance.store(image, imageData: data, forKey: robo)
                    self.roboList.insert(robo, atIndex: 0)
                    self.delegate?.collectionDidUpdate()
                }
            }
        }
    }
    
    func deleteRobo(robo: Robo) {
        ImageCache.sharedInstance.removeImage(forKey: robo)
        if let roboIndex = self.roboList.indexOf(robo) {
            self.roboList.removeAtIndex(roboIndex)
            self.delegate?.collectionDidUpdate()
        }
    }
    
    func persist() {
        NSUserDefaults.standardUserDefaults().setObject(roboList, forKey: kUserDefaultsRoboListKey)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
}
