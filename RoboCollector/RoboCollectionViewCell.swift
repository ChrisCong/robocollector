//
//  RoboCollectionViewCell.swift
//  RoboGen
//
//  Created by Christopher Cong on 4/21/16.
//  Copyright © 2016 ChrisCong. All rights reserved.
//

import UIKit

class RoboCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
}
