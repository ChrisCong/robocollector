//
//  RoboDetailViewController.swift
//  RoboCollector
//
//  Created by Christopher Cong on 4/21/16.
//  Copyright © 2016 ChrisCong. All rights reserved.
//

import UIKit

class RoboDetailViewController: UIViewController {
    @IBOutlet var imageView: UIImageView!
    
    var robo: Robo = ""
    lazy var deleteButton: UIBarButtonItem = UIBarButtonItem(title: "Delete",
                                                        style: .Plain,
                                                        target: self,
                                                        action: #selector(deletePressed(_:)))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = ImageCache.sharedInstance.image(forKey: robo)
        navigationItem.title = robo
        navigationItem.rightBarButtonItem = deleteButton
    }
    
    func deletePressed(sender: AnyObject) {
        
    }
}
