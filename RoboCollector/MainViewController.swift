//
//  ViewController.swift
//  RoboCollector
//
//  Created by Christopher Cong on 4/21/16.
//  Copyright © 2016 ChrisCong. All rights reserved.
//

import UIKit
import Alamofire

enum RoboSet: String{
    case RoboSet1 = "set1"
    case RoboSet2 = "set2"
    case RoboSet3 = "set3"
}

//MARK: MainViewController
class MainViewController: UIViewController {
    //MARK: Properties
    @IBOutlet var createTextField: UITextField!
    @IBOutlet var roboCollectionView: UICollectionView!
    
    private var collection = RoboCollection()
    private var set: RoboSet = .RoboSet1
    private let roboReuseIdentifier = "roboCell"
    private let cellsPerRow = 4
    private let sectionInsets = UIEdgeInsets(top: 0, left: -1, bottom: 0, right: -1)
    private let segueIdentifierShowRoboDetail = "showRoboDetail"
    
    //MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        collection.delegate = self
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == segueIdentifierShowRoboDetail, let detailVC = segue.destinationViewController as? RoboDetailViewController {
            if let indexPath = sender as? NSIndexPath {
                detailVC.robo = collection[indexPath.row]
            }
        }
    }
}

//MARK: RoboCollectionDelegate
extension MainViewController: RoboCollectionDelegate {
    func collectionDidUpdate() {
        self.roboCollectionView.reloadData()
    }
}

//MARK: UICollectionViewDataSource
extension MainViewController: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collection.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell: RoboCollectionViewCell
        if let _cell = collectionView.dequeueReusableCellWithReuseIdentifier(roboReuseIdentifier, forIndexPath: indexPath) as? RoboCollectionViewCell {
            cell = _cell
        } else {
            cell = RoboCollectionViewCell()
        }
        
        cell.imageView.image = ImageCache.sharedInstance.image(forKey: collection[indexPath.row])
        return cell
    }
}

//MARK: UICollectionViewDelegate
extension MainViewController: UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self .performSegueWithIdentifier(segueIdentifierShowRoboDetail, sender: indexPath)
    }
}

//MARK: UICollectionViewDelegateFlowLayout
extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        // 4 cells per row
        let width = collectionView.frame.size.width / CGFloat(cellsPerRow)
        return CGSize(width: width, height: width)
    }
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}

//MARK: UITextFieldDelegate
extension MainViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if var text = textField.text {
            text = text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            collection.addRobo(text, set: set)
        }
        
        textField.text = ""
        textField.resignFirstResponder()
        return true
    }
}